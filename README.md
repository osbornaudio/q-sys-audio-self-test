# README #

System Self Test by Wayne Osborn

### Overview ###
The purpose of this module is to provide an audio end-to-end self test feature that can be bolted into any design.
The Self Test block can be added to any design that utilises mics and speakers. Ensure your mic signals are routed to the test Input router and the test output router outputs are mixed into your output paths.
The idea is to perform end-to-end audio testing of a system out of hours and ensure all mics and speaker zones are functional, at least from a level perspective.

I am sharing this work with the community as pay back for the loads of valuable input and help I have received while here. Its relatively new and fresh and may have bugs, use at your risk.

### Input Tests ###
Produces a pre-recorded test message (or pink noise) to the configured speaker system in the room. 
Each microphone input is "listened to" to determine if audio is detected.
Pass/fail is determined for each mic input.

### Output Tests ###
Produces a pre-recorded test message (or pink noise) to each speaker system in the room. 
A configured microphone input is "listened to" to determine if audio is detected.
Pass/fail is determined for each speaker output zone.

### Email Report ###
The resulting test report is piped to the Emailer component to alert the test result.

### Comments ###
Tests may be scheduled to occur out of hours or they may be manually triggered.

### Contact ###
Wayne Osborn
+61432683728
wayne@osbornaudio.com.au